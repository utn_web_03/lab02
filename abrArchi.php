<?php
$archivo = $_GET['arch'];

//Inicio la sesión
session_start();



//Utiliza los datos de sesion comprueba que el usuario este autenticado
if ($_SESSION["autenticado"] != "SI") {
	header("Location: index.php");
	exit(); //fin del script		
}


//Declara ruta carpeta del usuario
$ruta = getenv('HOME_PATH').'/'.$_SESSION["usuario"].'/'.$archivo;



//header("Content-type: application/pdf");
if (is_dir($ruta) || $archivo == "atras") {

	if ($archivo != "atras") {
		$_SESSION["usuario"] = $_SESSION["usuario"] . '/' . $archivo;
		header("Location: carpetas.php");
	} else {
		$directorios = explode('/', $_SESSION["usuario"]);
		$newDirectory = str_replace('/' . end($directorios), '', $_SESSION["usuario"]);
		$_SESSION["usuario"] = $newDirectory;
		header("Location: carpetas.php");
	}
} else {

	$file =  fopen($ruta, "rb");
	$contenido =  fread($file, filesize($ruta));
	$mime =  mime_content_type($ruta);

	if ($mime == 'application/pdf') {
		header("Content-type: " . $mime);
		echo $contenido;
	} else if ($mime == 'image/png' || $mime == 'image/jpeg') {
		header("Content-type: ". $mime);
		echo $contenido; 

	} else {
		header("Content-Disposition: attachment; filename=" . $archivo);
		header("Content-type: " . $mime);
		header("Content-length: " . filesize($ruta));
		readfile($ruta);
	}
}
