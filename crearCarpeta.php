<?php
//Inicio la sesión
session_start();

//Utiliza los datos de sesion comprueba que el usuario este autenticado
if ($_SESSION["autenticado"] != "SI") {
    header("Location: index.php");
    exit(); //fin del scrip
}


$ruta  = 'D:\\McGregor\\TXT\\'.$_SESSION["usuario"]; // getenv('HOME_PATH'). $_SESSION["usuario"];
$error = false;

if (isset($_POST['createFolder'])) {
    if (isset($_POST['folderName']) && $_POST['folderName'] != "") {

        echo $ruta.'/'.$_POST['folderName'];
        mkdir($ruta.'/'.$_POST['folderName'], 777);
        header("Location: carpetas.php");
    } else {
        $error = true;
    }
}


?>
<!doctype html>
<html>

<head>
    <?php include_once('partes/encabe.inc'); ?>
    <title>Ingreso al Sitio</title>
</head>

<body class="container cuerpo">
    <header class="row">
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <img src="imagenes/encabe.png" alt="logo institucional" width="100%">
            </div>
        </div>
        <div class="row">
            <?php include_once('partes/menu.inc'); ?>
        </div>
        <br />
    </header>






    <main class="row">
        <form action="" method="POST">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong>Crear Carpeta</strong>
                </div>
                <div>
                    <div class="mb-3">
                        <input type="text" class="form-control" id="folderName" name="folderName" placeholder="Inserte el nombre de la carpeta" require="true">

                    </div>

                </div>
            </div>
            <?php if ($error) {
                echo 'El campo es requerido*';
            } ?>
            <div style="display: flex; justify-content: flex-end;">
                <input type="submit" name="createFolder" class="btn btn-primary" value="Crear" />
            </div>
        </form>
    </main>




    <footer class="row">

    </footer>
    <?php include_once('partes/final.inc'); ?>
</body>

</html>