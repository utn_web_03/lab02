<?php
//Inicio la sesión
session_start();

//Utiliza los datos de sesion comprueba que el usuario este autenticado
if ($_SESSION["autenticado"] != "SI") {
	header("Location: index.php");
	exit(); //fin del scrip
}


$ruta  = getenv('HOME_PATH').'/'.$_SESSION["usuario"];
$datos = explode('/', 'D:/McGregor/TXT/' . $_SESSION["usuario"]);
$ruta2 = '/' . $datos[1] . '/' . $_SESSION["usuario"];
?>
<!doctype html>
<html>

<head>
	<?php include_once('partes/encabe.inc'); ?>
	<title>Ingreso al Sitio</title>
</head>

<body class="container cuerpo">
	<header class="row">
		<div class="row">
			<div class="col-lg-6 col-sm-6">
				<img src="imagenes/encabe.png" alt="logo institucional" width="100%">
			</div>
		</div>
		<div class="row">
			<?php include_once('partes/menu.inc'); ?>
		</div>
		<br />
	</header>






	<main class="row">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<strong>Mi Cajón de Archivos</strong>
			</div>
			<div class="panel-body">
				<!-- <img src="./imagenes/icon/file.png" alt="icono" style="width: 24px; height: 24px;"> -->

				<?php
				$conta = 0;
				$directorio = opendir($ruta);

				echo '<div style="
					 display: flex;
					 justify-content: space-between;"> 
					 <a href=./agrearchi.php>
					 <img src="./imagenes/icon/file.png" alt="icono" style="width: 24px; height: 24px;">
					 Agregar archivo</a>
					 <a href=abrArchi.php?arch=atras>
					 <img src="./imagenes/icon/back.png" alt="icono" style="width: 37px; height: 24px;">
					 Atrás</a>
					 </div>';
				// echo '<div style="
				// display: flex;
				// justify-content: space-between;"> 
				// <a href=./agrearchi.php>Agregar archivo</a>
				// <input type="submit" name="back" value="Atrás" class="btn-custom" style="background: none !important;border: none; margin-top: 0.3rem;	position: relative; left: -0.5rem;"> </div>';

				echo '<br><br>';
				echo '<table class="table table-striped">';
				echo '<tr>';
				echo '<th>Nombre</th>';
				echo '<th>Tama&ntilde;o</th>';
				echo '<th>Borrar</th>';
				echo '</tr>';
				while ($elem = readdir($directorio)) {
					if (($elem != '.') and ($elem != '..')) {
						$explode =  explode('.', $elem);

						echo '<tr>';
						if (count($explode) > 1 && end($explode) == 'pptx' || end($explode) == 'ppt') {
							echo '<th><img src="./imagenes/icon/PowerPoint.png" alt="icono power point" style="width: 24px; height: 24px;" ><a href=abrArchi.php?arch=' . $elem . '>' . $elem . '</a></th>';
						} else if (count($explode) > 1 && end($explode) == 'docx') {
							echo '<th><img src="./imagenes/icon/word.png" alt="icono word" style="width: 24px; height: 24px;" ><a href=abrArchi.php?arch=' . $elem . '>' . $elem . '</a></th>';
						} else if (count($explode) > 1 && end($explode) == 'png' || end($explode) == 'jpg' || end($explode) == 'jpeg') {
							echo '<th><img src="./imagenes/icon/image.png" alt="icono image" style="width: 24px; height: 24px;" ><a href=abrArchi.php?arch=' . $elem . '>' . $elem . '</a></th>';
						} else if (count($explode) > 1 && end($explode) == 'pdf') {
							echo '<th><img src="./imagenes/icon/pdf.png" alt="icono image" style="width: 24px; height: 24px;" ><a href=abrArchi.php?arch=' . $elem . '>' . $elem . '</a></th>';
						} else {
							// echo '<th style="display:flex;"><img src="./imagenes/icon/file.png" alt="icono file" style="width: 24px; height: 24px;"><div ' . 'openFile("' . $elem . '")' . '><input style="background: none !important;
							// border: none; margin-top: 0.3rem;	position: relative; left: -0.5rem;" class="btn-custom" type="submit" name="file" value="' . $elem . '"   / ></div></th>';

							echo '<th><img src="./imagenes/icon/file.png" alt="icono file" style="width: 24px; height: 24px;" ><a href=abrArchi.php?arch=' . $elem . '>' . $elem . '</a></th>'; //Línea original
						}
						echo '<th>' . number_format(filesize($ruta . '/' . $elem) / 1000000, 2) . ' Mb</th>'; // peso
						echo '<th><a href=./codigos/borarchi.php?archi=' . $elem . '>Hacer</a></th>'; // para borrar
						echo '</tr>';
						$conta++;
					} // fin del if	  
				} // fin del while
				echo '</table>';
				echo '<br>';

				closedir($directorio);
				if ($conta == 0)
					echo 'La carpeta del usuario se encuetra vac&iacute;a';

				echo '<div style="display: flex;justify-content: flex-end;">
					<button class="btn btn-primary" onclick="createNewFolder()">Nueva Carpeta</button>
				</div>';
				?>
			</div>
		</div>
	</main>




	<footer class="row">

	</footer>
	<?php include_once('partes/final.inc'); ?>

	<script>
		function createNewFolder() {
			window.location = 'crearCarpeta.php';
		}
	</script>
</body>

</html>